from django.contrib import admin
from django.urls import path, include
from personajes.views import PersonajeListCreateAPIView
from peliculas.views import PeliculaListCreateAPIView


urlpatterns = [
    path('admin/', admin.site.urls),
    path('personajes/', PersonajeListCreateAPIView.as_view(), name='Personajelist'),
    path('peliculas/', PeliculaListCreateAPIView.as_view(), name='Peliculalist')
]
