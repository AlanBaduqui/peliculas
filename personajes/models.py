from django.db import models

# Create your models here.


class Personaje(models.Model):
    nombre = models.CharField(max_length=150)
    nombre_Real = models.CharField(max_length=150)
    imagen = models.ImageField()
    descripcion = models.TextField()
