from django.shortcuts import render

# Create your views here.
from rest_framework.generics import ListCreateAPIView
from personajes.models import Personaje
from personajes.serializers import PersonajeSerializer


class PersonajeListCreateAPIView(ListCreateAPIView):
    queryset = Personaje.objects.all()
    serializer_class = PersonajeSerializer
