from rest_framework.serializers import ModelSerializer
from personajes.models import Personaje


class PersonajeSerializer(ModelSerializer):
    class Meta:
        model = Personaje
        fields = '__all__'
