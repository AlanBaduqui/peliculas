from rest_framework.serializers import ModelSerializer
from peliculas.models import Pelicula


class PeliculaSerializers(ModelSerializer):
    class Meta:
        model = Pelicula
        fields = '__all__'
