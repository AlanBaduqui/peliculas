from django.db import models
from personajes.models import Personaje
# Create your models here.

class Pelicula(models.Model):
    nombre = models.CharField(max_length=150)
    año = models.DateField()
    descripcion = models.TextField()
    perosnaje = models.ForeignKey(Personaje, blank=True, on_delete=models.CASCADE)