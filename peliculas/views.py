from django.shortcuts import render

# Create your views here.

from rest_framework.generics import ListCreateAPIView
from peliculas.models import Pelicula
from peliculas.serializers import PeliculaSerializers

class PeliculaListCreateAPIView(ListCreateAPIView):
    queryset = Pelicula.objects.all()
    serializer_class = PeliculaSerializers
