from django.contrib import admin

# Register your models here.
from peliculas.views import Pelicula

admin.site.register(Pelicula)