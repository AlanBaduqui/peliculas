# Generated by Django 2.2 on 2020-04-03 22:15

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('personajes', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Pelicula',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=150)),
                ('año', models.DateField()),
                ('descripcion', models.TextField()),
                ('perosnaje', models.ForeignKey(blank=True, on_delete=django.db.models.deletion.CASCADE, to='personajes.Personaje')),
            ],
        ),
    ]
